<?php
/**
 * Description :
 * This class allows to define token module bootstrap class.
 * Token module bootstrap allows to boot token module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_user_profile\token\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\user_profile\token\model\UserProfileTokenKeyEntityFactory;
use people_sdk\module_user_profile\token\library\ConstToken;



class TokenBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: User profile token key entity factory instance.
     * @var UserProfileTokenKeyEntityFactory
     */
    protected $objUserProfileTokenKeyEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param UserProfileTokenKeyEntityFactory $objUserProfileTokenKeyEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        UserProfileTokenKeyEntityFactory $objUserProfileTokenKeyEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objUserProfileTokenKeyEntityFactory = $objUserProfileTokenKeyEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstToken::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set user profile token key entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'user_profile', 'token_key', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objUserProfileTokenKeyEntityFactory->getTabConfig(), $tabConfig);
            $this->objUserProfileTokenKeyEntityFactory->setTabConfig($tabConfig);
        };
    }



}