<?php

use people_sdk\module_user_profile\token\boot\TokenBootstrap;



return array(
    'people_user_profile_token_bootstrap' => [
        'call' => [
            'class_path_pattern' => TokenBootstrap::class . ':boot'
        ]
    ]
);