<?php

use people_sdk\user_profile\token\model\UserProfileTokenKeyEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'user_profile' => [
            'token_key' => [
                // User profile token entity key factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see UserProfileTokenKeyEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * User profile entity factory execution configuration array format:
                     * @see UserProfileTokenKeyEntityFactory::setTabUserProfileEntityFactoryExecConfig() configuration format.
                     */
                    'user_profile_factory_execution_config' => []
                ]
            ]
        ]
    ]
);