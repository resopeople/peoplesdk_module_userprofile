<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\user_profile\token\model\UserProfileTokenKeyEntityCollection;
use people_sdk\user_profile\token\model\UserProfileTokenKeyEntityFactory;
use people_sdk\user_profile\token\model\repository\UserProfileTokenKeyEntitySimpleRepository;
use people_sdk\user_profile\token\model\repository\UserProfileTokenKeyEntityMultiRepository;
use people_sdk\user_profile\token\model\repository\UserProfileTokenKeyEntityMultiCollectionRepository;



return array(
    // User profile token key entity services
    // ******************************************************************************

    'people_user_profile_token_key_entity_collection' => [
        'source' => UserProfileTokenKeyEntityCollection::class
    ],

    'people_user_profile_token_key_entity_factory_collection' => [
        'source' => UserProfileTokenKeyEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_user_profile_token_key_entity_factory' => [
        'source' => UserProfileTokenKeyEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_user_profile_token_key_entity_factory_collection'],
            ['type' => 'class', 'value' => UserProfileEntityFactory::class],
            ['type' => 'config', 'value' => 'people/user_profile/token_key/factory/user_profile_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_user_profile_token_key_entity_simple_repository' => [
        'source' => UserProfileTokenKeyEntitySimpleRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_user_profile_token_key_entity_multi_repository' => [
        'source' => UserProfileTokenKeyEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_user_profile_token_key_entity_multi_collection_repository' => [
        'source' => UserProfileTokenKeyEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_user_profile_token_key_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_user_profile_token_key_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    UserProfileTokenKeyEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_user_profile_token_key_entity_collection']
    ]
);