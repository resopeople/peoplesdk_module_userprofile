<?php
/**
 * Description :
 * This class allows to define attribute module bootstrap class.
 * Attribute module bootstrap allows to boot attribute module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_user_profile\attribute\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\user_profile\attribute\model\UserProfileAttrEntityFactory;
use people_sdk\module_user_profile\attribute\library\ConstAttribute;



class AttributeBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: User profile attribute entity factory instance.
     * @var UserProfileAttrEntityFactory
     */
    protected $objUserProfileAttrEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param UserProfileAttrEntityFactory $objUserProfileAttrEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        UserProfileAttrEntityFactory $objUserProfileAttrEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objUserProfileAttrEntityFactory = $objUserProfileAttrEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstAttribute::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set user profile attribute entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'user_profile', 'attribute', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objUserProfileAttrEntityFactory->getTabConfig(), $tabConfig);
            $this->objUserProfileAttrEntityFactory->setTabConfig($tabConfig);
        };
    }



}