<?php

use people_sdk\module_user_profile\attribute\boot\AttributeBootstrap;



return array(
    'people_user_profile_attribute_bootstrap' => [
        'call' => [
            'class_path_pattern' => AttributeBootstrap::class . ':boot'
        ]
    ]
);