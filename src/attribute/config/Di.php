<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\user_profile\attribute\model\UserProfileAttrEntityCollection;
use people_sdk\user_profile\attribute\model\UserProfileAttrEntityFactory;
use people_sdk\user_profile\attribute\model\repository\UserProfileAttrEntitySimpleRepository;
use people_sdk\user_profile\attribute\model\repository\UserProfileAttrEntitySimpleCollectionRepository;
use people_sdk\user_profile\attribute\model\repository\UserProfileAttrEntityMultiRepository;
use people_sdk\user_profile\attribute\model\repository\UserProfileAttrEntityMultiCollectionRepository;
use people_sdk\user_profile\attribute\provider\model\UserProfileAttrProvider;
use people_sdk\module_user_profile\attribute\helper\UserProfileAttrProviderHelper;



return array(
    // User profile attribute entity services
    // ******************************************************************************

    'people_user_profile_attr_entity_collection' => [
        'source' => UserProfileAttrEntityCollection::class
    ],

    'people_user_profile_attr_entity_factory_collection' => [
        'source' => UserProfileAttrEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_user_profile_attr_entity_factory' => [
        'source' => UserProfileAttrEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_user_profile_attr_entity_factory_collection']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_user_profile_attr_entity_simple_repository' => [
        'source' => UserProfileAttrEntitySimpleRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_user_profile_attr_entity_simple_collection_repository' => [
        'source' => UserProfileAttrEntitySimpleCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_user_profile_attr_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_user_profile_attr_entity_simple_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_user_profile_attr_entity_multi_repository' => [
        'source' => UserProfileAttrEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_user_profile_attr_entity_multi_collection_repository' => [
        'source' => UserProfileAttrEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_user_profile_attr_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_user_profile_attr_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // User profile attribute provider services
    // ******************************************************************************

    'people_user_profile_attr_entity_provider_collection' => [
        'source' => UserProfileAttrEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_user_profile_attr_provider' => [
        'source' => UserProfileAttrProvider::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_user_profile_attr_entity_provider_collection'],
            ['type' => 'config', 'value' => 'people/user_profile/attribute/provider/config'],
            ['type' => 'dependency', 'value' => 'people_user_profile_attr_provider_cache_repository']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // User profile attribute helper services
    // ******************************************************************************

    'people_user_profile_attr_provider_helper' => [
        'source' => UserProfileAttrProviderHelper::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_user_profile_attr_provider'],
            ['type' => 'dependency', 'value' => 'people_user_profile_attr_entity_simple_collection_repository']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Preferences
    // ******************************************************************************

    UserProfileAttrEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_user_profile_attr_entity_collection']
    ]
);