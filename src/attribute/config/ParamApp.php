<?php

use people_sdk\user_profile\attribute\model\UserProfileAttrEntityFactory;
use people_sdk\user_profile\attribute\provider\model\UserProfileAttrProvider;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'user_profile' => [
            'attribute' => [
                // User profile attribute entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see UserProfileAttrEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ]
                ],

                // User profile attribute provider
                'provider' => [
                    /**
                     * Configuration array format:
                     * @see UserProfileAttrProvider configuration format.
                     */
                    'config' => []
                ]
            ]
        ]
    ]
);