<?php
/**
 * Description :
 * This class allows to define user profile attribute provider helper class.
 * User profile attribute provider helper allows to provide features,
 * for user profile attribute provider.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_user_profile\attribute\helper;

use liberty_code\library\bean\model\DefaultBean;

use people_sdk\user_profile\attribute\library\ToolBoxUserProfileAttrRepository;
use people_sdk\user_profile\attribute\provider\model\UserProfileAttrProvider;
use people_sdk\user_profile\attribute\model\repository\UserProfileAttrEntitySimpleCollectionRepository;



class UserProfileAttrProviderHelper extends DefaultBean
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: User profile attribute provider instance.
     * @var UserProfileAttrProvider
     */
    protected $objUserProfileAttrProvider;



    /**
     * DI: User profile attribute entity collection repository instance.
     * @var UserProfileAttrEntitySimpleCollectionRepository
     */
    protected $objUserProfileAttrEntityCollectionRepo;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param UserProfileAttrProvider $objUserProfileAttrProvider
     * @param UserProfileAttrEntitySimpleCollectionRepository $objUserProfileAttrEntityCollectionRepo
     */
    public function __construct(
        UserProfileAttrProvider $objUserProfileAttrProvider,
        UserProfileAttrEntitySimpleCollectionRepository $objUserProfileAttrEntityCollectionRepo
    )
    {
        // Init properties
        $this->objUserProfileAttrProvider = $objUserProfileAttrProvider;
        $this->objUserProfileAttrEntityCollectionRepo = $objUserProfileAttrEntityCollectionRepo;

        // Call parent constructor
        parent::__construct();
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Reset user profile attribute provider.
     */
    public function reset()
    {
        // Clear cache, if required
        if(!is_null($objCacheRepo = $this->objUserProfileAttrProvider->getObjCacheRepo()))
        {
            $objCacheRepo->removeTabItem($objCacheRepo->getTabSearchKey());
        };

        // Clear collection
        $objUserProfileAttrEntityCollection = $this->objUserProfileAttrProvider->getObjAttributeCollection();
        $objUserProfileAttrEntityCollection->removeItemAll();
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Load user profile attributes, to get specified schemas,
     * available for user profile attribute provider.
     * Return true if success, false if an error occurs.
     *
     * User profile attribute entity collection repository execution configuration array format:
     * @see ToolBoxUserProfileAttrRepository::loadSchema() configuration array format.
     *
     * @param boolean $boolSchemaRequired = false
     * @param boolean $boolSchemaPrivateGetRequired = false
     * @param boolean $boolSchemaPrivateUpdateRequired = false
     * @param boolean $boolSchemaPublicGetRequired = false
     * @param boolean $boolSchemaPublicExtendGetRequired = false
     * @param null|array $tabUserProfileAttrEntityCollectionRepoExecConfig = null
     * @return boolean
     */
    public function loadSchema(
        $boolSchemaRequired = false,
        $boolSchemaPrivateGetRequired = false,
        $boolSchemaPrivateUpdateRequired = false,
        $boolSchemaPublicGetRequired = false,
        $boolSchemaPublicExtendGetRequired = false,
        array $tabUserProfileAttrEntityCollectionRepoExecConfig = null
    )
    {
        // Reset provider
        $this->reset();

        // Return result
        return ToolBoxUserProfileAttrRepository::loadSchema(
            $this->objUserProfileAttrProvider->getObjAttributeCollection(),
            $this->objUserProfileAttrEntityCollectionRepo,
            $boolSchemaRequired,
            $boolSchemaPrivateGetRequired,
            $boolSchemaPrivateUpdateRequired,
            $boolSchemaPublicGetRequired,
            $boolSchemaPublicExtendGetRequired,
            $tabUserProfileAttrEntityCollectionRepoExecConfig
        );
    }



}