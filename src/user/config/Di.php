<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\user_profile\attribute\provider\model\UserProfileAttrProvider;
use people_sdk\user_profile\user\attribute\value\model\UserProfileAttrValueEntityFactory;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\user_profile\user\model\repository\UserProfileEntitySimpleRepository;
use people_sdk\user_profile\user\model\repository\UserProfileEntityMultiRepository;
use people_sdk\user_profile\user\model\repository\UserProfileEntityMultiCollectionRepository;
use people_sdk\module_user_profile\attribute\helper\UserProfileAttrProviderHelper;
use people_sdk\module_user_profile\user\helper\UserProfileFactoryHelper;



return array(
    // User profile attribute value entity services
    // ******************************************************************************

    'people_user_profile_attr_value_entity_factory' => [
        'source' => UserProfileAttrValueEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'class', 'value' => UserProfileAttrProvider::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // User profile entity services
    // ******************************************************************************

    'people_user_profile_entity_factory' => [
        'source' => UserProfileEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'mixed', 'value' => null],
            ['type' => 'dependency', 'value' => 'people_user_profile_attr_value_entity_factory'],
            ['type' => 'config', 'value' => 'people/user_profile/factory/user_profile_attribute_value_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/user_profile/factory/subject_permission_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/user_profile/factory/role_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_user_profile_entity_simple_repository' => [
        'source' => UserProfileEntitySimpleRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class],
            ['type' => 'config', 'value' => 'people/user_profile/repository/boundary']
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_user_profile_entity_multi_repository' => [
        'source' => UserProfileEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class],
            ['type' => 'config', 'value' => 'people/user_profile/repository/boundary']
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_user_profile_entity_multi_collection_repository' => [
        'source' => UserProfileEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_user_profile_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_user_profile_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // User profile helper services
    // ******************************************************************************

    'people_user_profile_factory_helper' => [
        'source' => UserProfileFactoryHelper::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_user_profile_entity_factory'],
            ['type' => 'class', 'value' => UserProfileAttrProviderHelper::class]
        ],
        'option' => [
            'shared' => true
        ]
    ]
);