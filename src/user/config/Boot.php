<?php

use people_sdk\module_user_profile\user\boot\UserBootstrap;



return array(
    'people_user_profile_user_bootstrap' => [
        'call' => [
            'class_path_pattern' => UserBootstrap::class . ':boot'
        ]
    ]
);