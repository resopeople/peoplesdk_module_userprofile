<?php

use people_sdk\user_profile\user\model\UserProfileEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'user_profile' => [
            // User profile entity factory
            'factory' => [
                /**
                 * Configuration array format:
                 * @see UserProfileEntityFactory configuration format.
                 */
                'config' => [
                    'select_entity_require' => true,
                    'select_entity_create_require' => true,
                    'select_entity_collection_set_require' => true
                ],

                /**
                 * User profile attribute value entity factory execution configuration array format:
                 * @see UserProfileEntityFactory::setTabUserProfileAttrValueEntityFactoryExecConfig() configuration format.
                 */
                'user_profile_attribute_value_factory_execution_config' => [],

                /**
                 * Subject permission entity factory execution configuration array format:
                 * @see UserProfileEntityFactory::setTabSubjPermEntityFactoryExecConfig() configuration format.
                 */
                'subject_permission_factory_execution_config' => [],

                /**
                 * Role entity factory execution configuration array format:
                 * @see UserProfileEntityFactory::setTabRoleEntityFactoryExecConfig() configuration format.
                 */
                'role_factory_execution_config' => []
            ],

            // User profile entity repository
            'repository' => [
                /**
                 * Boundary format:
                 * @var string
                 */
                'boundary' => ''
            ]
        ]
    ]
);