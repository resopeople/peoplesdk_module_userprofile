<?php
/**
 * Description :
 * This class allows to define user profile factory helper class.
 * User profile factory helper allows to provide features,
 * for user profile entity factory.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_user_profile\user\helper;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\library\crypto\library\ToolBoxHash;
use people_sdk\user_profile\user\model\UserProfileEntityCollection;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\module_user_profile\attribute\helper\UserProfileAttrProviderHelper;



class UserProfileFactoryHelper extends DefaultBean
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: User profile entity factory instance.
     * @var UserProfileEntityFactory
     */
    protected $objUserProfileEntityFactory;



    /**
     * DI: User profile attribute provider helper instance.
     * @var UserProfileAttrProviderHelper
     */
    protected $objUserProfileAttrProviderHelper;



    /**
     * Associative array of user profile entity collection instances, used for schema.
     * @var UserProfileEntityCollection[]
     */
    protected $tabUserProfileEntityCollection;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc

     * @param UserProfileEntityFactory $objUserProfileEntityFactory
     * @param UserProfileAttrProviderHelper $objUserProfileAttrProviderHelper
     */
    public function __construct(
        UserProfileEntityFactory $objUserProfileEntityFactory,
        UserProfileAttrProviderHelper $objUserProfileAttrProviderHelper
    )
    {
        // Init properties
        $this->objUserProfileEntityFactory = $objUserProfileEntityFactory;
        $this->objUserProfileAttrProviderHelper = $objUserProfileAttrProviderHelper;
        $this->tabUserProfileEntityCollection = array();

        // Call parent constructor
        parent::__construct();
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Load user profile attributes, to get specified schemas,
     * available for user profile entity factory.
     * Return true if success, false if an error occurs.
     *
     * User profile attribute entity collection repository execution configuration array format:
     * @see UserProfileAttrProviderHelper::loadSchema()
     * user profile attribute entity collection repository execution configuration array format.
     *
     * @param boolean $boolSchemaRequired = false
     * @param boolean $boolSchemaPrivateGetRequired = false
     * @param boolean $boolSchemaPrivateUpdateRequired = false
     * @param boolean $boolSchemaPublicGetRequired = false
     * @param boolean $boolSchemaPublicExtendGetRequired = false
     * @param null|array $tabUserProfileAttrEntityCollectionRepoExecConfig = null
     * @return boolean
     */
    public function loadSchema(
        $boolSchemaRequired = false,
        $boolSchemaPrivateGetRequired = false,
        $boolSchemaPrivateUpdateRequired = false,
        $boolSchemaPublicGetRequired = false,
        $boolSchemaPublicExtendGetRequired = false,
        array $tabUserProfileAttrEntityCollectionRepoExecConfig = null
    )
    {
        // Init provider
        $result = $this->objUserProfileAttrProviderHelper->loadSchema(
            $boolSchemaRequired,
            $boolSchemaPrivateGetRequired,
            $boolSchemaPrivateUpdateRequired,
            $boolSchemaPublicGetRequired,
            $boolSchemaPublicExtendGetRequired,
            $tabUserProfileAttrEntityCollectionRepoExecConfig
        );

        // Get user profile entity collection
        $strKey = ToolBoxHash::getStrHash(array(
            $boolSchemaRequired,
            $boolSchemaPrivateGetRequired,
            $boolSchemaPrivateUpdateRequired,
            $boolSchemaPublicGetRequired,
            $boolSchemaPublicExtendGetRequired
        ));
        if(!array_key_exists($strKey, $this->tabUserProfileEntityCollection))
        {
            $this->tabUserProfileEntityCollection[$strKey] = new UserProfileEntityCollection();
        }
        $objUserProfileEntityCollection = $this->tabUserProfileEntityCollection[$strKey];

        // Init factory
        $this->objUserProfileEntityFactory->setObjEntityCollection($objUserProfileEntityCollection);

        // Return result
        return $result;
    }



}