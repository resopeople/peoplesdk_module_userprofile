<?php
/**
 * Description :
 * This class allows to define API module bootstrap class.
 * API module bootstrap allows to boot API module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_user_profile\api\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\user_profile\api\model\UserProfileApiKeyEntityFactory;
use people_sdk\module_user_profile\api\library\ConstApi;



class ApiBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: User profile API key entity factory instance.
     * @var UserProfileApiKeyEntityFactory
     */
    protected $objUserProfileApiKeyEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param UserProfileApiKeyEntityFactory $objUserProfileApiKeyEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        UserProfileApiKeyEntityFactory $objUserProfileApiKeyEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objUserProfileApiKeyEntityFactory = $objUserProfileApiKeyEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstApi::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set user profile API key entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'user_profile', 'api_key', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objUserProfileApiKeyEntityFactory->getTabConfig(), $tabConfig);
            $this->objUserProfileApiKeyEntityFactory->setTabConfig($tabConfig);
        };
    }



}