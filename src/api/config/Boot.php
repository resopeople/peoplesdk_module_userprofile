<?php

use people_sdk\module_user_profile\api\boot\ApiBootstrap;



return array(
    'people_user_profile_api_bootstrap' => [
        'call' => [
            'class_path_pattern' => ApiBootstrap::class . ':boot'
        ]
    ]
);