<?php

use people_sdk\user_profile\api\model\UserProfileApiKeyEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'user_profile' => [
            'api_key' => [
                // User profile API key entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see UserProfileApiKeyEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * User profile entity factory execution configuration array format:
                     * @see UserProfileApiKeyEntityFactory::setTabUserProfileEntityFactoryExecConfig() configuration format.
                     */
                    'user_profile_factory_execution_config' => []
                ]
            ]
        ]
    ]
);