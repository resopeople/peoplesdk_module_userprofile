<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\user_profile\user\model\UserProfileEntityFactory;
use people_sdk\user_profile\api\model\UserProfileApiKeyEntityCollection;
use people_sdk\user_profile\api\model\UserProfileApiKeyEntityFactory;
use people_sdk\user_profile\api\model\repository\UserProfileApiKeyEntitySimpleRepository;
use people_sdk\user_profile\api\model\repository\UserProfileApiKeyEntityMultiRepository;
use people_sdk\user_profile\api\model\repository\UserProfileApiKeyEntityMultiCollectionRepository;



return array(
    // User profile API key entity services
    // ******************************************************************************

    'people_user_profile_api_key_entity_collection' => [
        'source' => UserProfileApiKeyEntityCollection::class
    ],

    'people_user_profile_api_key_entity_factory_collection' => [
        'source' => UserProfileApiKeyEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_user_profile_api_key_entity_factory' => [
        'source' => UserProfileApiKeyEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_user_profile_api_key_entity_factory_collection'],
            ['type' => 'class', 'value' => UserProfileEntityFactory::class],
            ['type' => 'config', 'value' => 'people/user_profile/api_key/factory/user_profile_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_user_profile_api_key_entity_simple_repository' => [
        'source' => UserProfileApiKeyEntitySimpleRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_user_profile_api_key_entity_multi_repository' => [
        'source' => UserProfileApiKeyEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_user_profile_api_key_entity_multi_collection_repository' => [
        'source' => UserProfileApiKeyEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_user_profile_api_key_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_user_profile_api_key_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    UserProfileApiKeyEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_user_profile_api_key_entity_collection']
    ]
);