<?php

use people_sdk\user_profile\requisition\request\info\factory\model\UserProfileConfigSndInfoFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'requisition' => [
            'request' => [
                'snd_info_factory' => [
                    'config' => [
                        'snd_info_factory' => [
                            ['snd_info_factory' => 'people_user_profile_requisition_request_snd_info_factory']
                        ]
                    ]
                ]
            ]
        ],

        'user_profile' => [
            'requisition' => [
                'request' => [
                    // User profile requisition request sending information factory
                    'snd_info_factory' => [
                        /**
                         * Configuration array format:
                         * @see UserProfileConfigSndInfoFactory configuration format.
                         */
                        'config' => [
                            'user_profile_support_type' => 'header',
                            'user_profile_current_profile_include_config_key' => 'people_user_profile_current_profile_include',
                            'user_profile_role_support_type' => 'header',
                            'user_profile_role_perm_full_update_config_key' => 'people_user_profile_role_perm_full_update',
                            'user_profile_role_role_full_update_config_key' => 'people_user_profile_role_role_full_update'
                        ]
                    ]
                ]
            ]
        ]
    ]
);