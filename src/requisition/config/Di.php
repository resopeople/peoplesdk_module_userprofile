<?php

use people_sdk\user_profile\requisition\request\info\factory\model\UserProfileConfigSndInfoFactory;



return array(
    // User profile requisition request sending information services
    // ******************************************************************************

    'people_user_profile_requisition_request_snd_info_factory' => [
        'source' => UserProfileConfigSndInfoFactory::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_config'],
            ['type' => 'config', 'value' => 'people/user_profile/requisition/request/snd_info_factory/config']
        ],
        'option' => [
            'shared' => true
        ]
    ]
);