<?php

use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\format\model\FormatData;
use liberty_code\cache\repository\format\model\FormatRepository;



return array(
    // User profile cache services
    // ******************************************************************************

    'people_user_profile_cache_register' => [
        'source' => DefaultTableRegister::class,
        'argument' => [
            ['type' => 'config', 'value' => 'people/user_profile/cache/register/config'],
            ['type' => 'mixed', 'value' => null]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_user_profile_cache_repository' => [
        'source' => FormatRepository::class,
        'argument' => [
            ['type' => 'config', 'value' => 'people/user_profile/cache/config'],
            ['type' => 'dependency', 'value' => 'people_user_profile_cache_register'],
            ['type' => 'class', 'value' => FormatData::class],
            ['type' => 'class', 'value' => FormatData::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // User profile attribute provider cache services
    // ******************************************************************************

    'people_user_profile_attr_provider_cache_register' => [
        'source' => DefaultTableRegister::class,
        'set' => ['type' => 'dependency', 'value' => 'people_user_profile_cache_register'],
        'option' => [
            'shared' => true
        ]
    ],

    'people_user_profile_attr_provider_cache_repository' => [
        'source' => FormatRepository::class,
        'argument' => [
            ['type' => 'config', 'value' => 'people/user_profile/attribute/provider/cache/config'],
            ['type' => 'dependency', 'value' => 'people_user_profile_attr_provider_cache_register'],
            ['type' => 'class', 'value' => FormatData::class],
            ['type' => 'class', 'value' => FormatData::class]
        ],
        'option' => [
            'shared' => true
        ]
    ]
);