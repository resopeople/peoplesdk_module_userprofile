<?php

use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\format\model\FormatRepository;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'user_profile' => [
            // User profile cache configuration
            'cache' => [
                'register' => [
                    /**
                     * Configuration array format:
                     * @see DefaultTableRegister configuration format.
                     */
                    'config' => []
                ],

                /**
                 * Configuration array format:
                 * @see FormatRepository configuration format.
                 */
                'config' => [
                    'key_pattern' => 'people-user-profile-%s',
                    'key_regexp_select' => '#people\-user\-profile\-(.+)#'
                ]
            ],

            'attribute' => [
                'provider' => [
                    // User profile attribute provider cache configuration
                    'cache' => [
                        /**
                         * Configuration array format:
                         * @see FormatRepository configuration format.
                         */
                        'config' => [
                            'key_pattern' => 'people-user-profile-attr-provider-%s',
                            'key_regexp_select' => '#people\-user\-profile\-attr\-provider\-(.+)#'
                        ]
                    ]
                ]
            ]
        ]
    ]
);