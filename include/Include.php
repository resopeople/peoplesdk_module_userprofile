<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/attribute/library/ConstAttribute.php');
include($strRootPath . '/src/attribute/helper/UserProfileAttrProviderHelper.php');
include($strRootPath . '/src/attribute/boot/AttributeBootstrap.php');

include($strRootPath . '/src/user/library/ConstUser.php');
include($strRootPath . '/src/user/helper/UserProfileFactoryHelper.php');
include($strRootPath . '/src/user/boot/UserBootstrap.php');

include($strRootPath . '/src/api/library/ConstApi.php');
include($strRootPath . '/src/api/boot/ApiBootstrap.php');

include($strRootPath . '/src/token/library/ConstToken.php');
include($strRootPath . '/src/token/boot/TokenBootstrap.php');

include($strRootPath . '/src/requisition/library/ConstRequisition.php');

include($strRootPath . '/src/cache/library/ConstCache.php');